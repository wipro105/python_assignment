#10.	Given a number line from -infinity to +infinity. You start at 0 and can go either to the left or to the right. The condition is that in i’th move, you take i steps. In the first move take 1 step, second move 2 steps and so on.
#Hint: 3 can be reached in 2 steps (0, 1) (1, 3). 2 can be reached in 3 steps (0, 1) (1,-1) (-1, 2)



source = 0
dest = 89
temp = []
arr = [0]
step = 0
print("Processing...")
while True:
    step+=1
    for num in arr:
        source = num + step
        if abs(source) <= abs(dest): temp.append(source)
        source = num - step
        if abs(source) <= abs(dest): temp.append(source)
        #print "step",step,"\t",temp
        #If you want to see the process unblock the above line
        if dest in temp: break
    arr = temp[:]
    if dest in temp: break
    temp[:] = []

print("\nThe shortest distance to %d in incremental steps is: %d"%(dest,step))

