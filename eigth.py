#8.	Given a string, str1=""”Python is a widely used high-level programming language for general-purpose programming, created by Guido van Rossum and first released in 1991. An interpreted language, Python has a design philosophy which emphasizes code readability (notably using whitespace indentation to delimit code blocks rather than curly braces or keywords), and a syntax which allows programmers to express concepts in fewer lines of code than possible in languages such as C++ or Java. The language provides constructs intended to enable writing clear programs on both a small and large scale .Python features a dynamic type system and automatic memory management and supports multiple programming paradigms, including object-oriented, imperative, functional programming, and procedural styles. It has a large and comprehensive standard library. Python interpreters are available for many operating systems, allowing Python code to run on a wide variety of systems. CPython, the reference implementation of Python, is open source software and has a community-based development model, as do nearly all of its variant implementations. CPython is managed by the non-profit Python Software Foundation."""
#Hint:  Assume that the first word is preceded by " "
#a.	Build a dictionary where the key is a word and the value is the list of words that are likely to follow.
#i.	E.g. Python  [is, has, features, interpreters, code, Software]. In this example the words listed are likely to follow “Python”
#b.	Given a word predict the word that’s likely to follow.

str = "Python is a widely Python Python used high-level programming language " \
      "for general-purpose programming, created by Guido van Rossum and first " \
      "released in 1991. An interpreted language, Python has a design philosophy" \
      " which emphasizes code readability (notably using whitespace indentation" \
      " to delimit code blocks rather than curly braces or keywords), and a syntax" \
      " which allows programmers to express concepts in fewer lines of code than" \
      " possible in languages such as C++ or Java. The language provides constructs" \
      " intended to enable writing clear programs on both a small and large scale " \
      "Python features a dynamic type system and automatic memory management and supports " \
      "multiple programming paradigms, including object-oriented, imperative," \
      " functional programming, and procedural styles. It has a large and comprehensive" \
      " standard library. Python interpreters are available for many operating systems," \
      " allowing Python code to run on a wide variety of systems. CPython, the reference" \
      " implementation of Python, is open source software and has a community-based" \
      " development model, as do nearly all of its variant implementations. CPython is" \
      " managed by the non-profit Python Software Foundation."

dict = {}
i = j = 0

str = str.lower()
str = str.replace("," or "." or "(" or ")", "")
str = str.strip().split(" ")
str1 = str[:]

while i in range(0,len(str)):
    j = 0
    dict[str[i]] = []
    while j in range(0,len(str1)):
        if str[i] == str1[j]:
            dict[str[i]].append(str1[j+1])
        j+=1
        if j+1 == len(str1):
            break
    i+=1

for item in dict:
    print(item," ", dict[item])

user = input("\nEnter the word to which you want to find the next word")

if user in dict:
    print(dict[user][0])

